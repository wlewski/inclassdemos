/* Notes
 - Method is a function inside of a class
 - function is a set of instructions that is not in a class.
 - using namespace std; - grabbing the standard namespace. If this is done you do not need to do std::

 Terms
 - << or >> stream indicator - used to direct code
 - :: - get out of namespace
 - cout - console output
 - cin - console input
 - std - standard namespace
 - _getch(); - used to keep window open
 - x++ - Preincrementing, increments x after the rest of the line is run
 - ++x - Postincrementing, increments x before it does the rest of the line

 Escape Characters
 - \a - Alert
 - \b - Backspace
 - \e - Escape character
 - \f - Formfeed page break
 - \r - Carriage return
 - \n - New line
 - \t - Horizontal tab
 - \v - Vertical Tab
 - \\ - Backslash
 - \' - Apostrophe
 - \" - Double quote mark
 - \? - Question mark

 Datatypes
 - int i; - integer named i
 - float f; - float named f
 - double d; - double named d
 - bool b; - boolean named b
 - char c; - character named c

 */
#include <iostream>
#include <conio.h>

using namespace std;

int main()
{
	// Constant declaration because PI never changes
	const double PI = 3.1415926;

	int i;

	// Writing to the console and then taking user inputted info back into the program 
	std::cout << "Hello world!\n";
	std::cout << "Enter an integer: ";
	std::cin >> i;

	// If Statements /
	if (i > 100) cout << "That's a big number.\n";
	else cout << "That's not a big number\n";

	if (i) cout << "I is not zero";

	// Switch Case example online 

	// For Loop 
	for (int i = 0; i <= 10; i++)
	{
		cout << i << "\n";
	}

	// While Loop //
	cout << "Do you want to do math? (y/n): ";
	int x = 1;
	char input;
	cin >> input;

	while (input == 'y')
	{
		cout << "9 X " << x << " = " << 9 * x << "\n";
		cout << "Again?: ";
		cin >> input;
		x++;
	}

	// Do While Loop //
	do
	{
		cout << "9 X " << x << " = " << 9 * x << "\n";
		cout << "Again?: ";
		cin >> input;
		x++;
	} while (input == 'y');

	_getch();
	return 0;
}