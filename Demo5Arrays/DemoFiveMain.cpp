/* Array Notes
 - Declaring an array
   - the first position in an array starts with 0 and the last is one shorter than the length of the array
   - int arrAges[10]; - declares an array with 10 ages
   - int arrAges[10] = {1,2,3,4,5,6,7,8,9,10}; - can declare an array and set all values at once too
	 - When initializing this way you dont need a number in the [] but it is recommened
 - Can create arrays that hold data for Structs
 - Creating an array on the heap requires having a pointer but can be used to get an
   array the size a user wants
 - Character Arrays
   - This is what a string is, an array of characters
   - when creating a character array you want to do one more than the length of your
	 string because you need a null character '\0'
   - You need a null character because when printing to the console with a character
	 array you cout needs to have a null character to know where to stop
   - char name[5] = { 'W', 'i', 'l', 'l', '\0' }; how to stucture character array
   - char name[5] = "Will"; can also do it in double quotes like a sting
   - Can convert a sting to a character array by doing string_name.c_str()
 - Two Dimensional Arrays
   - Allows you to have multiple rows and columns in an array
   - char two_dimensional[8][8]; this would create a two dimensional 8x8 array
 - Vectors
   - These are a type of array that allow you to continually enter data with no
	 fixed size to the array.

 - Iterator - is a type of pointer that iterates through a collection.

 */

#include <iostream>
#include <conio.h>
#include <string>
#include <vector>

using namespace std;

struct Person
{
	string name;
	int age;
};

void PrintNumbers(int* pArray, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << pArray[i] << ", ";
	}
}

int main()
{
	// Array on the Stack //
	/*const int NUM_AGES = 5;
	int arrAges[NUM_AGES];

	arrAges[0] = 10;
	arrAges[1] = 20;
	arrAges[2] = 30;
	arrAges[3] = 40;
	arrAges[4] = 50;

	// Adding 1 to each value in an Array. //
	for (int i = 0; i < NUM_AGES; i++)
	{
		arrAges[i]++;
		cout << arrAges[i] << "\n";
	}*/


	// Structing Data with Arrays //
	/*const int NUM_PEOPLE = 5;
	string arrNames[NUM_PEOPLE] = { "James", "Jack", "Ian", "Robin", "Laura" };
	int arrAges[NUM_PEOPLE] = { 20, 30, 19, 18, 23 };

	Person people[NUM_PEOPLE];

	for (int i = 0; i < NUM_PEOPLE; i++)
	{
		people[i].name = arrNames[i];
		people[i].age = arrAges[i];

		cout << people[i].name << "'s age is " << people[i].age << "\n";
	}*/


	// Creating an Array on the Heap //
	/*int numPeople = 0;

	cout << "How many people would you like in the array: ";
	cin >> numPeople;

	int *ages = new int[numPeople];

	for (int i = 0; i < numPeople; i++)
	{
		cout << "What is the age for person " << (i + 1) << ": ";
		cin >> ages[i];
	}
	for (int i = 0; i < numPeople; i++)
	{
		cout << ages[i] << "\n";
	}*/


	// Character Arrays //
	/*char name[5] = { 'W', 'i', 'l', 'l', '\0' };
	cout << name;*/

	// Two-Dimensional Arrays //
	/*const int COLS = 8;
	const int ROWS
	char checkers[ROWS][COLS] = {
		{ ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W' },
		{ 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ' },
		{ ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W' },
		{ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
		{ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
		{ 'B', ' ', 'B', ' ', 'B', ' ', 'B', ' ' },
		{ ' ', 'B', ' ', 'B', ' ', 'B', ' ', 'B' },
		{ 'B', ' ', 'B', ' ', 'B', ' ', 'B', ' ' }
	};

	// Prints out array
	for (int y = 0; y < ROWS; y++)
	{
		for (int x = 0; x < COLS; x++)
		{
			cout << checkers[y][x];
		}
		cout << "\n";
	}*/


	// Vectors //
	/*char input = 'y';
	Person person;
	vector<Person> people; // Creating a vector

	while (input == 'y')
	{
		cout << "Name: ";
		cin >> person.name;

		cout << "Age: ";
		cin >> person.age;

		people.push_back(person); // Adding to the vector

		cout << "Would you like to add another person. (y/n): ";
		cin >> input;
	}*/

	// Iterate through a Vector //
	// Slow (but easy)
	/*for (int i = 0; i < people.size(); i++)
	{
		cout << people[i].name << "\n";
	}*/

	// Fast (but syntax is hard to remember)
	/*vector<Person>::iterator it; // Iterator class inside of vector class
	for (it = people.begin(); it != people.end(); it++)
	{
		cout << it->name << "\n";
	}*/

	// Passing an array as a Parameter //
	/*const int NUMS_COUNT = 5;
	int nums[NUMS_COUNT] = { 1, 2, 3, 4, 5 };
	PrintNumbers(nums, NUMS_COUNT);*/


	// Sum of GPA's //
	/*const int NUM_GPAS = 3;
	float gpas[NUM_GPAS];
	for (int i = 0; i < NUM_GPAS; i++)
	{
		cout << "Enter a gpa for a student: ";
		cin >> gpas[i];
	}
	float sum = 0;
	for (int i = 0; i < NUM_GPAS; i++)
	{
		cout << gpas[i] << ", ";
		sum += gpas[i];
	}*/


	_getch();
	return 0;
}