/*******************************************************
 Pointer Demo

 Pointer - variables that holds an address
 Deferencing - getting value that pointer is pointing to
 Stack Memory -
  - Student s; //If in an if statement
 Heap Memory -
  - Student *pstudent = new Student;

 * used for creating and deferencing pointers
 & "address of" operator

 Creating Pointers
  - int *pI; //Perferred way of writing it
  - int* pI;
  - int * pI;

 //Fix SDK
 //project -> properties -> windows SDK
 *******************************************************/

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

struct Student
{
	string name;
	float gpa;
};

void Square(int* pNumber)
{
	int num = *pNumber; // get the number at address stored in pNumber
	int squared = num * num; // square it

	*pNumber = squared; // dereference *pNumber and assign the value of squared to *pNumber

	// Same Functionality as 3 lines above 
	//*pNumber *= *pNumber; 
}

void PrintStudentData(Student* pStudent)
{
	cout << pStudent->name << ": " << pStudent->gpa << "\n";
}

/*******************************
 Function that returns a Pointer
 *******************************/
Student* CreateStudent(string name, float gpa = 4.0f)
{
	/* Creates Student on the stack and then it is deleted from memory when functon ends.
	Student s;
	s.name = name;
	s.gpa = gpa;
	return s;
	*/

	// 
	Student* pS = new Student;
	//(*pS).name = name;
	pS->name = name; // -> does the same as the derefence the * and . - (*pS).
	pS->gpa = gpa;
	return pS;
}



int main()
{
	/*
	int i = 6;

	//int *pI = &i;
	int *pI; //Creating a pointer
	pI = &i; //setting pI equal to the address of variable i

	int x = *pI; // x is equal to 6

	cout << "The address of i is " << &i << "\n"; //Displaying the the address in memory
	cout << "The value of pI is " << pI << "\n";

	//i++; // Next line of code will now print out 7
	//(*pI)++; // Does the same as i++;
	//pI++ // This would increase the pointers address
	cout << "The value pI points to is " << *pI << "\n"; //Getting value of i
	*/


	/*****************************
	 Passing a variable by address
	 *****************************/
	int i = 5;
	Square(&i); // address of operator 
	cout << i;


	/******************
	Working with Memory
	 ******************/
	char input;
	cout << "Create a student? (y/n): ";
	cin >> input;

	Student* pStudent = nullptr; // this only holds an address about 32-64 bits

	// int* pI, pJ, pK - creates pI as a pointer and everything else as an integer

	//If false then a new instance of Student will not be created in memory
	if (input == 'y' || input == 'Y')
	{
		pStudent = CreateStudent("William");
	}

	if (pStudent)
	{
		PrintStudentData(pStudent);
		delete pStudent;
	}

	_getch();
	return 0;
}