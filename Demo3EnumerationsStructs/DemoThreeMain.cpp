/*************************************************************************************************
 - Enumeration - finite set of values
	- similar to classes where you can use it as a datatype once created
	- When running a code with enumerations in it, the names will be replaced with integers
 - Structs - how you would group code
	- Can be used in C#
	- Similar to Classes
	- if you are trying to group data use a struct, if you're trying to store methods use a class
 *************************************************************************************************/

// Enumerations and Struct //
#include <iostream>
#include <conio.h>

using namespace std;

enum DoorState
{
	Closed, Opening, Open, Closing
};

struct Door
{
	DoorState state;
	bool isLocked;
};

int main()
{
	Door door1;
	door1.isLocked = true;
	if (door1.state == Open)
	{

	}



	_getch();
	return 0;
}
