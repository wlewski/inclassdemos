/*********************************************************************************************************************************
	- can use a xml file to have a code snippet
	- xml file can go into visual studio folder
	   - code snippits
	   - whatever programming language
	   - my code snippets

	- dont need public or private for functions
	- C++ compiles top of file to bottom
	- never call main() other than the start of a program

	// Definitons //
	- method - function inside of a class
	- functions - a type of procedure or routine
	- calling a function - a request made by a program or script that performs a predetermined function
	- envoking a function - fancy way of saying call function
	- function prototype - a declaration of a function that tells the program about the type of the value
	  returned by the function along with the number and type of arguments. the prototype declaration looks
	  just like a function definition except it has no body.
	- parameter - example: printName(int count){ }
	- argument - example: printName(3)
	- return types - can return 1 or 0
	   - void - returns nothing back
	   - int - returns an integer
	- tuple - grouping of datatypes - not in C++
	- recursion - a function that calls itself until it doesnt - can do what loops do but loops cant always do what recursion can
	- stack - section of memory
	- stack overflow - when the stack of memory overflows
	- reference parameter - can make a variable a reference parameter by adding an ampersand (&) character
		- can do reference parameter instead of value with variables, which allows the variable to be changed between functions

	// Helpful Tools //
	- breakpoint - stops execution
	- F11 - can be used to go through line by line in code while running
	- ctrl shift b - builds without running, will get rid of errors
	- ctrl k c - comment out
	- control k u - uncomment
 *********************************************************************************************************************************/
#include <iostream>
#include <conio.h>

using namespace std;

// Function Prototypes
void printName(int count = 3); 
int add(int& a, int  b); // reference parameter example
bool divide(float num, float denom, float& answer);
void countDownFrom(int number);

int main()
{
	//int x = 2;
	//int y = 6;

	//printName(add(x,y)); 

	//cout << x << "\n"; // returns x + 1 because of reference parameter
	//cout << y << "\n"; // returns y because there is no reference parameter 


	// Example of Reference Parameter //
	/*float num1 = 0;
	float num2 = 0;
	float ans = 0;

	cout << "Enter two numbers: ";
	cin >> num1;
	cin >> num2;

	if (divide(num1, num2, ans)) cout << num1 << " divided by " << num2 << " is " << ans << "\n";
	else cout << "You cannot divide by zero!\n";*/

	char input = 'y';
	while (input == 'y')
	{
		countDownFrom(10);
		cout << "Again? (y/n): ";
		cin >> input;
	}

	_getch();
	return 0;
}

// Recursive Function 
void countDownFrom(int number)
{
	cout << number << "\n";
	if (number == 0) return; // if number != 0 then it steps down to countDownFrom()
	countDownFrom(number - 1);
}

// Divide Function
bool divide(float num, float denom, float& answer) // use of reference parameter
{
	if (denom == 0) return false;

	answer = num / denom;
	return true;
}

// Addition Function
int add(int& a, int  b) 
{
	a++;
	b++;

	return a + b;
}

// Print Name Function
void printName(int count)
{
	for (int i = 0; i < count; i++) {
		cout << "Will Lewandowski\n";
	}
}
